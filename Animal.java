public class Animal {
    private String animalName;
    private boolean isCarnivore;
    private double topSpeed;

    public Animal(String animalName, Boolean isCarnivore, double topSpeed) {
        this.animalName = animalName;
        this.isCarnivore = isCarnivore;
        this.topSpeed = topSpeed;
    }

    //GETTERS
    public String getAnimalName() {
        return this.animalName;
    }
    public boolean getIsCarnivore() {
        return this.isCarnivore;
    }
    public double getTopSpeed() {
        return this.topSpeed;
    }
    //SETTERS
    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }

    public boolean canAnimalCatchGazelle() {
        return (this.isCarnivore && this.topSpeed >= 75);
    }

    public void displayAnimalInfo() {
        //Print all fields 
        System.out.println("Animal name: " + this.animalName);
        
        if (this.isCarnivore)
            System.out.println("Animal diet: Carnivore");
        else if (!this.isCarnivore)
            System.out.println("Animal diet: Herbivore");

        System.out.println("Animal top speed: " + this.topSpeed + "km/h");
    }
}