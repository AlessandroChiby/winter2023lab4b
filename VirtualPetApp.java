import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {
        System.out.print("\033[H\033[2J"); //clear the console
        System.out.flush();  

        Scanner reader = new Scanner(System.in);

        Animal[] felines = new Animal[2];
        
        /*felines[0] = new Animal("Cheetah", true, 110);
        felines[1] = new Animal("Tiger", true, 65);
        felines[2] = new Animal("Lion", true, 74);
        felines[3] = new Animal("Jaguar", true, 80);
        */
        boolean isCarnivore;

        for (int i = 0; i < felines.length; i++) {
            System.out.print("\nEnter animal name: ");
            String name = reader.nextLine();

            System.out.print("\nIs animal carnivore? (Y/N): ");
            String diet = reader.nextLine().toLowerCase();

            if (diet.equals("y"))
                isCarnivore = true;
            else 
                isCarnivore = false;

            System.out.print("\nEnter animal top speed: ");
            double topSpeed = reader.nextDouble();
            reader.nextLine();
            
            //Create animal at each index
            felines[i] = new Animal(name, isCarnivore, topSpeed);

            //System.out.print("\033[H\033[2J");  
        }
        felines[felines.length - 1].displayAnimalInfo();

        //update
        System.out.print("\nUpdate animal top speed: ");
        felines[felines.length - 1].setTopSpeed(reader.nextDouble());

        felines[felines.length - 1].displayAnimalInfo();
    }
}
